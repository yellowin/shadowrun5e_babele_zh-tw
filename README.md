## shadowrun5e-FoundryVTT的正體中文版，包括角色卡。

## 安装方法
- (0.8.6)使用連結手動安裝，清單檔位址 [https://gitlab.com/yellowin/shadowrun5e_babele_zh-tw/raw/master/module.json](https://gitlab.com/yellowin/shadowrun5e_babele_zh-tw/raw/master/module.json)

- (0.7.10)使用連結手動安裝，清單檔位址 [https://gitlab.com/yellowin/shadowrun5e_babele_zh-tw/raw/master/module_0710.json](https://gitlab.com/yellowin/shadowrun5e_babele_zh-tw/raw/master/module_0710.json)

使用該中文化模組，需要禁用或卸載其他核心系統中文化模組（遊戲系統的中文化模組不影響）。

## 翻譯協力：麻糬，ponpon，yellowin，歐蕾
